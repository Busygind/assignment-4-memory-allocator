//
// Created by dmitryb on 12/8/22.
//

#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <inttypes.h>

#define START_TEST(test, num)  {                       \
    printf("==================Test %"PRId64"==================\n", (num) + 1);\
}

#define END_TEST()  {                       \
    println_message("==========================================\n");    \
}

typedef bool (*test)();
void run_tests();

#endif //ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
