#ifndef _UTIL_H_
#define _UTIL_H_

#include <stddef.h>
#include <stdio.h>

inline size_t size_max( size_t x, size_t y ) { return (x >= y)? x : y ; }

_Noreturn void err( const char* msg, ... );

void print_newline(void);
void println_error(const char * message);
void println_message(const char * message);

#endif
