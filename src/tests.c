//
// Created by dmitryb on 12/8/22.
//

#define _DEFAULT_SOURCE

#include "tests.h"
#include <sys/mman.h>

#define HEAP_SZ REGION_MIN_SIZE
#define NUM_OF_TESTS 5

static void heap_free(void* heap) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = HEAP_SZ}).bytes);
}

bool test_simple_memory_allocate() {
    void* heap_pointer = heap_init(HEAP_SZ);
    debug_heap(stdout, heap_pointer);
    println_message("Starting allocation...");
    void* array = _malloc(1000);
    debug_heap(stdout, heap_pointer);
    if (!array) {
        println_error("Allocation failed!");
        return false;
    }
    _free(array);
    heap_free(heap_pointer);
    return true;
}

bool test_free_one_block_from_several() {
    void* heap_pointer = heap_init(HEAP_SZ);
    debug_heap(stdout, heap_pointer);
    println_message("Starting allocation...");
    void* arr1 = _malloc(1000);
    void* arr2 = _malloc(1000);
    void* arr3 = _malloc(1000);
    debug_heap(stdout, heap_pointer);
    println_message("Starting freeing a second block...");
    _free(arr2);
    debug_heap(stdout, heap_pointer);
    _free(arr1);
    _free(arr3);
    heap_free(heap_pointer);
    return true;
}

bool test_free_two_blocks_from_several() {
    void* heap_pointer = heap_init(HEAP_SZ);
    debug_heap(stdout, heap_pointer);
    println_message("Starting allocation...");
    void* arr1 = _malloc(1000);
    void* arr2 = _malloc(1000);
    void* arr3 = _malloc(1000);
    void* arr4 = _malloc(1000);
    void* arr5 = _malloc(1000);
    debug_heap(stdout, heap_pointer);
    println_message("Starting freeing a second and third blocks...");
    _free(arr2);
    _free(arr3);
    debug_heap(stdout, heap_pointer);
    _free(arr1);
    _free(arr4);
    _free(arr5);
    heap_free(heap_pointer);
    return true;
}

bool test_out_of_memory() {
    void* heap_pointer = heap_init(HEAP_SZ);
    debug_heap(stdout, heap_pointer);
    println_message("Starting allocation...");

    void* double_block = _malloc(HEAP_SZ * 2);
    debug_heap(stdout, heap_pointer);

    _free(double_block);
    heap_free(heap_pointer);

    return true;
}

bool test_out_of_memory_in_another_region() {

    void* heap_pointer = heap_init(HEAP_SZ);
    struct block_header* first_block_header = (struct block_header *) heap_pointer;
    debug_heap(stdout, heap_pointer);

    println_message("Starting allocation...");
    void* huge_block = _malloc(first_block_header->capacity.bytes);
    debug_heap(stdout, heap_pointer);

    struct block_header* block_header = (struct block_header*) (huge_block - offsetof(struct block_header, contents));

    void* after_heap = (void *) block_header->contents + block_header->capacity.bytes;
    void* buf_map = mmap(after_heap,
                          HEAP_SZ,
                          PROT_READ | PROT_WRITE,
                          MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS,
                          -1,
                          0);

    println_message("Start allocation in new region...");
    void* next_block = _malloc(1000);
    debug_heap(stdout, heap_pointer);

    _free(next_block);
    _free(huge_block);

    heap_free(buf_map);
    heap_free(heap_pointer);
    return true;
}

void run_tests() {
    test tests[] = {
            test_simple_memory_allocate,
            test_free_one_block_from_several,
            test_free_two_blocks_from_several,
            test_out_of_memory,
            test_out_of_memory_in_another_region
    };

    println_message("Start testing...\n");
    int64_t passed = 0;

    for (size_t i = 0; i < NUM_OF_TESTS; ++i) {
        START_TEST(tests[i], i);
        passed += tests[i]();
        END_TEST();
    }

    printf("Result: %"PRId64" of %"PRId32" tests successfully passed", passed, NUM_OF_TESTS);
}
