#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args); // NOLINT 
  va_end (args);
  abort();
}

void print_newline(void) {
    printf("%s", "\n");
}

void println_error(const char *const message) {
    fprintf(stderr, "%s", message);
    print_newline();
}

void println_message(const char *const message) {
    printf("%s", message);
    print_newline();
}

extern inline size_t size_max( size_t x, size_t y );
